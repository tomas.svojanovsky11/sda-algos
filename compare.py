class Node:
    def __init__(self, value):
        self.value = value
        self.left = None
        self.right = None

    def __repr__(self):
        return f"{self.value}"

"""
        Root - 🎄

        🎄
      /     \
    🎄        🎄
   /   \     /  \
  🎄    🎄    🎅🏼  🎄
"""

"""
        Root - 🎄

        🎄
      /     \
    🎄        🎅🏼
   /   \     /  \
  🎄    🎄   🎄   🎄
"""



# Strom 1

root1 = Node("🎄")
root1.left = Node("🎄")
root1.right = Node("🎅🏼")

# root1.left.left = Node("🎄")
# root1.left.right = Node("🎄")
#
# root1.right.left = Node("🎄")
# root1.right.right = Node("🎄")

# Strom 2


root2 = Node("🎄")
root2.left = Node("🎄")
root2.left.left = Node("🎅🏼")

# root2.left.left = Node("🎄")
# root2.left.right = Node("🎄")
#
# root2.right.left = Node("🎅🏼")
# root2.right.right = Node("🎄")


def compare(a, b):
    # strukturalni kontrola
    if a is None and b is None:
        return True

    # strukturalni kontrola
    if a is None or b is None:
        print("Tady taky nic")
        return False

    if a.value != b.value:
        print("Tady nic")
        return False

    return compare(a.left, b.left) and compare(a.right, b.right)


print(compare(root1, root2))
