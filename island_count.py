grid = [
    [1, 0, 0, 0, 0, 0],
    [1, 0, 0, 1, 1, 0],
    [0, 0, 0, 1, 1, 0],
    [1, 1, 0, 0, 0, 0],
    [1, 0, 0, 0, 1, 1],
    [1, 0, 0, 0, 0, 0],
]


# list = [1, 0, 0, 0, 0, 0]


# grid = []
# grid = [[]]

# [
# [False, False, False, False, False, False],
# [False, False, False, False, False, False],
# [False, False, False, False, False, False],
# [False, False, False, False, False, False],
# [False, False, False, False, False, False],
# [False, False, False, False, False, False],
# ]

def island_count(grid):
    if not grid and not grid[0]:
        return 0

    nums_rows = len(grid)
    nums_cols = len(grid[0])
    visited = [[False for _ in range(nums_cols)] for _ in range(nums_rows)]
    # visited = []
    #
    # for i in range(nums_rows):
    #     visited.append([])
    #
    #     for _ in range(nums_cols):
    #         visited[i].append(False)

    def dfs(row, col):
        # hranice - row < 0 or col < 0 or row >= nums_rows or col >= nums_cols
        # uz jsem tam byl
        # voda

        if row < 0 or col < 0 or row >= nums_rows or col >= nums_cols or grid[row][col] == 0 or visited[row][col]:
            print(row, col, "Basecase")
            return

        print(row, col, "Jo dobry")

        visited[row][col] = True

        dfs(row - 1, col)
        dfs(row + 1, col)
        dfs(row, col - 1)
        dfs(row, col + 1)

    count = 0

    for row in range(nums_rows):
        for col in range(nums_cols):
            print(row, col, "Pruchod")

            # ostrov a nebyl jsem tam jeste
            if grid[row][col] == 1 and not visited[row][col]:
                dfs(row, col)
                count += 1

    return count


print(island_count(grid))
