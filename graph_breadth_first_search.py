graph = {
    "a": ["c", "b"],
    "b": ["d"],
    "c": ["e"],
    "d": ["f"],
    "e": [],
    "f": [],
}


def breadth_first_search(graph, source, destination):
    queue = [source]

    while len(queue) > 0:
        current = queue.pop(0)

        if current == destination:
            return True

        for node in graph[current]:
            queue.append(node)

    return False


print(breadth_first_search(graph, "a", "e"))
