class Node:
    def __init__(self, value):
        self.value = value
        self.left = None
        self.right = None

    def __repr__(self):
        return f"{self.value}"


class BSTree:
    def __init__(self):
        self.root = None

    def insert(self, value):
        node = Node(value)

        if self.root is None:
            self.root = node
            return node

        current = self.root
        while True:
            if value < current.value:  # leva strana
                if current.left is None:  # uz jsem nasel to misto
                    current.left = node
                    return node
                else:
                    current = current.left  # jdu dal
            else:  # prava strana
                if current.right is None:  # uz jsem nasel to misto
                    current.right = node
                    return node
                else:
                    current = current.right  # jdu dal

    def search(self, value):
        if self.root is None:
            return None

        current = self.root

        while current:
            if value == current.value:
                return current

            if value < current.value:
                current = current.left
            else:
                current = current.right

        return None

    def delete(self, value):
        self.root = self.delete_node(self.root, value)

    def delete_node(self, root, value):
        if root is None:
            return None

        if value < root.value:
            root.left = self.delete_node(root.left, value)
        elif value > root.value:
            root.right = self.delete_node(root.right, value)
        else:
            # toto je pripad pro jeden nebo nula potomku
            if root.left is None:
                return root.right
            elif root.right is None:
                return root.left
            else:  # tady jsou dva potomci
                max_node = self.find_max(root.left)  # root je to co jsem chtel puvodne smazat
                root.value = max_node.value
                root.left = self.delete_node(root.left, max_node.value)

    # def find_min(self, node):
    #     while node.left is not None:
    #         node = node.left
    #
    #     return node

    def find_max(self, node):
        while node.right is not None:
            node = node.right

        return node


tree = BSTree()
tree.insert(100)
tree.insert(200)
tree.insert(50)
tree.insert(25)
tree.insert(75)

print(tree.search(150))

# print(tree.root.value == 100)
# print(tree.root.left.value == 50)
# print(tree.root.left.right.value == 75)
# print(tree.root.left.left.value == 25)
# print(tree.root.right.value == 200)
