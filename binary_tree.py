class Node:
    def __init__(self, value):
        self.value = value
        self.left = None
        self.right = None

    def __repr__(self):
        return f"{self.value}"


"""
        Root - 7

         7
      /     \
    23        8
   /   \     /  \
  5    4     21  15 
"""

root = Node(7)
root.left = Node(23)
root.right = Node(8)

root.left.left = Node(5)
root.left.right = Node(4)

root.right.left = Node(21)
root.right.right = Node(15)

# """
#         Root - 7
#
#          7
#       /     \
#     23        8
#    /   \     /  \
#   5    4     21  15
# """


# pre order - [7, 23, 5, 4, 8, 21, 15]


# def walk(curr, path):
#     if curr is None:
#         return path
#
#     path.append(curr)
#
#     walk(curr.left, path)
#     walk(curr.right, path)
#
#     return path
#
#
# print(walk(root, []))

# """
#         Root - 7
#
#          7
#       /     \
#     23        8
#    /   \     /  \
#   5    4     21  15
# """


# in order - [5, 23, 4, 7, 21, 8, 15]

# def walk(curr, path):  # curr je uzel 23
#     if curr is None:
#         return path
#
#     walk(curr.left, path)
#
#     path.append(curr)
#
#     walk(curr.right, path)
#
#     return path
#
#
# print(walk(root, []))

# post order - [5, 4, 23, 21, 15, 8, 7]

"""
        Root - 7

         7
      /     \
    23        8
   /   \     /  \
  5    4     21  15 
"""


# post order - [5, 4, 23, 21, 15, 8, 7]

def walk(curr, path):  # curr je uzel 23
    if curr is None:
        return path

    walk(curr.left, path)
    walk(curr.right, path)

    path.append(curr)

    return path


print(walk(root, []))
