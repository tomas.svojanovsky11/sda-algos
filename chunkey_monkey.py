# slices
# [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
# + nejake cislo - 2
# [[1, 2], [3, 4], [5, 6], [7, 8], [9, 10], [11]]

# nums = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
# start:stop:step

# print(nums[1:8:2])

# [[1, 2], [3, 4], [5, 6], [7, 8], [9, 10], [11]]
# [[1, 2, 3], [4, 5, 6], [7, 8, 9], [10, 11]]

def chunkey_monkey(data, size):  # funkce s dvema parametrama
    chunkey_list = []

    for i in range(0, len(data), size):
        chunk = data[i:i + size]
        chunkey_list.append(chunk)

    return chunkey_list


data = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]

# print(list(range(0, len(data), 3)))

print(chunkey_monkey(data, 4))

