def merge_sort(array):
    if len(array) > 1:
        left_array = array[:len(array) // 2]
        right_array = array[len(array) // 2:]

        merge_sort(left_array)
        merge_sort(right_array)

        i = 0
        j = 0
        k = 0

        while i < len(left_array) and j < len(right_array):
            if right_array[i] < left_array[j]:
                array[k] = left_array[i]
                i += 1
            else:
                array[k] = right_array[j]
                j += 1

            k += 1

        while i < len(left_array):
            array[k] = left_array[i]
            i += 1
            k += 1

        while j < len(right_array):
            array[k] = right_array[j]
            j += 1
            k += 1


array = [6, 5, 12, 10, 9, 1]

merge_sort(array)
print(array)

# def merge_sort(array):  # [6, 5, 12]
#     if len(array) > 1:  # base case
#         left_array = array[:len(array) // 2]
#         right_array = array[len(array) // 2:]
#
#         merge_sort(left_array)  # [6]
#         merge_sort(right_array)  # [5, 12]
#
#         i = 0
#         j = 0
#         k = 0
#
#         while i < len(left_array) and j < len(right_array):
#             if left_array[i] < right_array[j]:
#                 array[k] = left_array[i]
#                 i += 1  # leva cast posun o jedno
#             else:
#                 array[k] = right_array[j]
#                 j += 1  # prava cast posun o jedno
#
#             k += 1  # bila sipka pro puvodni list
#
#         while i < len(left_array):  # vyskladam co zustalo vlevo
#             array[k] = left_array[i]
#             i += 1
#             k += 1
#
#         while j < len(right_array):  # vyskladam co zustalo vpravo
#             array[k] = right_array[j]
#             j += 1
#             k += 1


# array = [6, 5, 12, 10, 9, 1]
#
# merge_sort(array)
# print(array)
