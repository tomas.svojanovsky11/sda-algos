from __future__ import annotations
import dataclasses


@dataclasses.dataclass
class Node:
    value: str
    next: Node | None = None
    prev: Node | None = None


# class Node:
#     def __init__(self, value):
#         self.value = value
#         self.next = None
#         self.prev = None
#
#     def __repr__(self):
#         return f"{self.value}"

class DoublyLinkedList:
    def __init__(self):
        self.head = None
        self.tail = None
        self.length = 0

    def prepend(self, value):
        node = Node(value)
        self.length += 1

        if self.head is None:
            self.head = self.tail = node
            return

        node.next = self.head
        self.head.prev = node
        self.head = node

    def append(self, value):
        node = Node(value)
        self.length += 1

        if self.tail is None:
            self.head = self.tail = node
            return

        node.prev = self.tail
        self.tail.next = node
        self.tail = node

    def remove_node(self, node: Node):
        self.length = max(0, self.length - 1)

        if self.length == 0:
            out = self.head.value if self.head else None  # prazdny nebo s jednim uzlem
            self.head = self.tail = None
            return out

        if node.prev:  # modra sipka
            node.prev.next = node.next

        if node.next:  # oranzova sipka
            node.next.prev = node.prev

        if node == self.head:
            self.head = node.next

        if node == self.tail:
            self.tail = node.prev

        node.next = None
        node.prev = None

        return node.value
