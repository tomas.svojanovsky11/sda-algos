from __future__ import annotations
import dataclasses


# homework - pridat vlozeni pozici

@dataclasses.dataclass
class Node:
    value: str
    next: Node | None = None
    #
    # def __init__(self, value):
    #     self.value = value
    #     self.next = None  # zelena sipka


class LinkedList:
    def __init__(self):
        self.head = None
        self.length = 0

    def prepend(self, value):
        node = Node(value)
        self.length += 1

        if self.head is None:  # kdyz je linkedlist prazdny
            self.head = node
            return

        # kdyz uz tam neco je
        node.next = self.head
        self.head = node

    def append(self, value):
        node = Node(value)
        self.length += 1

        if self.head is None:  # kdyz je linkedlist prazdny
            self.head = node
            return

        # skacu po vazbach az na konec
        last = self.head
        while last.next:
            last = last.next

        last.next = node  # pridam za posledni uzel novy uzel

    def insert_after(self, prev_node, value):
        new_node = Node(value)

        new_node.next = prev_node.next
        prev_node.next = new_node

    def delete_node(self, position):
        # if position < 0:
        #     raise Exception("Position je mensi nez 0")

        if self.head is None:
            return

        temp = self.head

        if position == 0:
            self.head = temp.next
            temp.next = None
            return

        for i in range(position - 1):
            temp = temp.next
            if temp is None:
                break

        if temp is None:
            return

        if temp.next is None:
            return

        next = temp.next.next
        temp.next = None
        temp.next = next

    def search(self, value):
        curr = self.head

        while curr is not None:
            if curr.value == value:
                return True

        return False

    def debug(self):
        curr = self.head
        out = ""

        for i in range(self.length):
            if curr is None:
                break

            out += f"{i} -> {curr.value} "
            curr = curr.next

        raise Exception("olalalala")

        # 0 -> A 1 -> B 2 -> C
        print(out)


linked_list = LinkedList()
linked_list.append("A")
linked_list.append("B")
linked_list.prepend("Z")
linked_list.debug()
#
# print(linked_list.head)
# print(linked_list.head.next)
# print(linked_list.head.next.next)

# node_a = Node("A")
# node_b = Node("B")
# node_c = Node("C")
# node_d = Node("D")
#
# node_a.next = node_b
# node_b.next = node_c
# node_c.next = node_d
#
# print(node_a)
# print(node_a.next)
# print(node_a.next.next)
# print(node_a.next.next.next)
