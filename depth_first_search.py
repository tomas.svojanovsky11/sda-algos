class Node:
    def __init__(self, value):
        self.value = value
        self.left = None
        self.right = None

    def __repr__(self):
        return f"{self.value}"


root = Node("🎄")
root.left = Node("🎄")
root.right = Node("🎄")

root.left.left = Node("🎄")
root.left.right = Node("🎄")

root.right.left = Node("🎅🏼")
root.right.right = Node("🎄")

# root = Node(7)
# root.left = Node(23)
# root.right = Node(8)
#
# root.left.left = Node(5)
# root.left.right = Node(4)
#
# root.right.left = Node(21)
# root.right.right = Node(15)



# def dfs_search(root, value):
#     if root is None:
#         return False
#
#     print(root)
#
#     if root.value == value:
#         return True
#
#     if dfs_search(root.left, value):
#         return True
#
#     if dfs_search(root.right, value):
#         return True
#
#     return False

def dfs_search(root, value):
    stack = [root]

    while len(stack):
        current = stack.pop()

        print(current)

        if current.value == value:
            return True

        # musim driv vpravo, protoze
        # to co je posledni pridane
        # jde prvni ven
        if current.right:
            stack.append(current.right)

        if current.left:
            stack.append(current.left)

    return False

"""
        Root - 🎄

        🎄
      /     \
    🎄        🎄
   /   \     /  \
  🎄    🎄    🎅🏼  🎄
"""

dfs_search(root, "🎅🏼")
