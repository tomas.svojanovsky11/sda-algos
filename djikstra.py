list1 = [None] * 7
#      (1) --- (4) ---- (5)
#    /  |  /    |      / |
# (0)   | ------|------- |
#    \  |/      |  /     |

#      (2) --- (3) ---- (6)

list1[0] = [
    {"to": 1, "weight": 3},
    {"to": 2, "weight": 1},
]
list1[1] = [
    {"to": 0, "weight": 3},
    {"to": 2, "weight": 4},
    {"to": 4, "weight": 1},
]
list1[2] = [
    {"to": 1, "weight": 4},
    {"to": 3, "weight": 7},
    {"to": 0, "weight": 1},
];
list1[3] = [
    {"to": 2, "weight": 7},
    {"to": 4, "weight": 5},
    {"to": 6, "weight": 1},
]
list1[4] = [
    {"to": 1, "weight": 1},
    {"to": 3, "weight": 5},
    {"to": 5, "weight": 2},
]
list1[5] = [
    {"to": 6, "weight": 1},
    {"to": 4, "weight": 2},
    {"to": 2, "weight": 18},
]
list1[6] = [
    {"to": 3, "weight": 1},
    {"to": 5, "weight": 1},
]


def has_unvisited(seen, distances):
    print(seen, distances)
    for i in range(len(seen)):
        if not seen[i] and distances[i] < float("inf"):
            return True

    return False


def get_lowest_unvisited(seen, distances):
    lowest_value = float("inf")
    index = -1

    for i in range(len(seen)):
        if seen[i]:
            continue

        if distances[i] < lowest_value:
            lowest_value = distances[i]
            index = i

    print(distances)
    print(lowest_value)

    return index


def djikstra_path_search(beginning, destination, grid):
    grid_size = len(grid)
    seen = [False] * grid_size
    distances = [float("inf")] * grid_size
    prev = [-1] * grid_size

    distances[beginning] = 0

    while has_unvisited(seen, distances):
        current = get_lowest_unvisited(seen, distances)
        seen[current] = True

        candidate = grid[current]
        for i in range(len(candidate)):
            edge = candidate[i]
            if seen[edge["to"]]:
                continue

            # to co jsme delali v tabulce - pripadne prohozeni hodnot
            distance = distances[current] + edge["weight"]
            if distance < distances[edge["to"]]:
                distances[edge["to"]] = distance
                prev[edge["to"]] = current

    result = []

    current = destination
    # jdu az do pocatecniho uzlu
    while prev[current] != -1:
        result.append(current)
        current = prev[current]

    result.append(beginning)
    result.reverse()

    return result


print(djikstra_path_search(0, 5, list1))