class MinHeap:
    def __init__(self):
        self.heap = []

    def get_parent(self, i):
        return (i - 1) // 2

    def get_left_child(self, i):
        return i * 2 + 1

    def get_right_child(self, i):
        return i * 2 + 2

    def insert(self, value):
        self.heap.append(value)
        self.heapify_up(len(self.heap) - 1)

    def heapify_up(self, i):
        parent_index = self.get_parent(i)
        parent_value = self.heap[parent_index]
        value = self.heap[i]

        if parent_value > value:
            self.heap[i] = parent_value
            self.heap[parent_index] = value
            self.heapify_up(parent_index)

    def heapify_down(self, index):
        smallest = index
        left_child_index = self.get_left_child(index)
        right_child_index = self.get_right_child(index)
        size = len(self.heap)

        # prvne se podivam vlevo, kdyz je to mensi tak si to priradim na smallest
        if left_child_index < size and self.heap[left_child_index] < self.heap[smallest]:
            smallest = left_child_index

        # potom se podivam vpravo
        if right_child_index < size and self.heap[right_child_index] < self.heap[smallest]:
            smallest = right_child_index

        if smallest != index:
            self.heap[index], self.heap[smallest] = self.heap[smallest], self.heap[index]
            self.heapify_down(smallest)




