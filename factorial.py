def factorial(n):  # 0
    if n == 0:  # base case
        return 1

    return n * factorial(n - 1)
# factorial 4 * factorial(4 - 1)

# 5! = 5 * 4 * 3 * 2 * 1

print(factorial(5))

# factorial(5) | 5 * 24 | 5
# factorial(4) | 4 * 6 | 4
# factorial(3) | 3 * 2| 3
# factorial(2) | 2 * 1| 2
# factorial(1) | 1 * 1 | 1
# factorial(0) | 1 | 0
