def partition(array, low, high):  # option + command + L
    # control + shift + L
    # control + alt + L
    pivot = array[high]

    i = low - 1  # modra sipka

    print(array, low, high)

    for j in range(low, high):  # zelena sipka
        # print("Prvek:", array[j], "Pivot", pivot)
        if array[j] <= pivot:
            i = i + 1

            array[i], array[j] = array[j], array[i]

    array[i + 1], array[high] = array[high], array[i + 1]

    return i + 1


def quick_sort(array, low, high):
    if low < high:
        pivot_position = partition(array, low, high)  # rozdelim si list na mensi  a vetsi hodnoty

        quick_sort(array, low, pivot_position - 1)  # opakuji znovu pro levou cast

        quick_sort(array, pivot_position + 1, high)  # opakuji znovu pro pravou cast


array = sorted([7, 2, 1, 8, 6, 3, 5, 4], reverse=True)

quick_sort(array, 0, len(array) - 1)
print(array)
