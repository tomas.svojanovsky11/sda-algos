def binary_search(needle, haystack):
    low = 0
    high = len(haystack) - 1

    while low <= high:
        mid = (low + high) // 2

        if haystack[mid] == needle:
            return mid
        elif haystack[mid] < needle:
            low = mid + 1
        else:
            high = mid - 1

    return -1


data = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

print(binary_search(8, data))
