class Node:
    def __init__(self, value):
        self.value = value
        self.left = None
        self.right = None

    def __repr__(self):
        return f"{self.value}"


root = Node(7)
root.left = Node(23)
root.right = Node(8)

root.left.left = Node(5)
root.left.right = Node(4)

root.right.left = Node(21)
root.right.right = Node(15)

"""
        Root - 7

         7
      /     \
    23        8
   /   \     /  \
  5    4     21  15 
"""


def breadth_search(root, value):  # root - 7, value - 21
    # simulace queue
    queue = [root]  # [15]

    while len(queue):
        # beru prvni prvek ze zacatku
        current = queue.pop(0)  # 21

        if current.value == value:  # 21 == 21
            return True

        if current.left:
            queue.append(current.left)

        if current.right:
            queue.append(current.right)

    return False


print(breadth_search(root, 7))

"""
    Root - 7

         7
      /     \
    23        8
   /   \     /  \
  5    4     21  15 
"""
