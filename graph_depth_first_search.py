graph = {
    "a": ["c", "b"],
    "b": ["d"],
    "c": ["e"],
    "d": ["f"],
    "e": [],
    "f": [],
}


# def depth_first_search(graph, source, destination):
#     stack = [source]  # a
#
#     while len(stack) > 0:
#         current = stack.pop()  # a
#
#         print(current)
#
#         if current == destination:
#             return True
#
#         for node in graph[current]:  # ["c", "b"]
#             stack.append(node)
#
#     return False

def depth_first_search(graph, source, destination):
    print(source)
    if source == destination:
        return True

    for node in graph[source]:
        depth_first_search(graph, node, destination)

    return False


print(depth_first_search(graph, "a", "x"))


