class Node:
    def __init__(self, value):
        self.value = value
        self.prev = None


class Stack:
    def __init__(self):
        self.length = 0
        self.head = None

    def push(self, value):
        new_node = Node(value)
        self.length += 1

        if self.head is None:
            self.head = new_node
            return

        new_node.prev = self.head
        self.head = new_node

    def pop(self):
        self.length = max(0, self.length - 1)

        if self.length == 0:
            head = self.head
            self.head = None
            return head.value if head else None

        head = self.head
        self.head = head.prev

        return head.value

    def peek(self):
        return self.head.value if self.head else None

    def print(self):
        node = self.head

        while node is not Node:
            print(node.value)
            node = node.prev


stack = Stack()
stack.push("A")
stack.push("B")
stack.push("C")
stack.pop()
print(stack.peek())  # B
