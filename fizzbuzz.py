# [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
# Pokud je cislo delitelne tremi tak vrat Fizz
# Pokud je cislo delitelne peti tak vrat Buzz
# Pokud je cislo delitelne tremi i peti tak vrat FizzBuzz

# [1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ... 15]

# [1,2, "Fizz", 4, "Buzz", "FizzBuzz"]

# Jak vygeneruji cisla od 1 do 100 aniz by to musel psat rucne?

# print(list(range(1, 101)))

# 9 : 3 = 3 zbytek 0
# 9 : 4 = 2 zbytek 1

def fizz_buzz():
    result = []

    for n in range(1, 101):
        if n % 3 == 0 and n % 5 == 0:
            result.append("FizzBuzz")
        elif n % 3 == 0:
            result.append("Fizz")
        elif n % 5 == 0:
            result.append("Buzz")
        else:
            result.append(n)

    return result


print(fizz_buzz())
