class Node:
    def __init__(self, value):
        self.value = value
        self.next = None


class Queue:
    def __init__(self):
        self.length = 0
        self.tail = None
        self.head = None

    def enqueue(self, value):
        new_node = Node(value)
        self.length += 1

        if self.head is None:
            self.head = self.tail = new_node
            return

        self.tail.next = new_node
        self.tail = new_node

    def dequeue(self):
        if self.head is None:
            return None

        self.length -= 1
        head = self.head
        self.head = self.head.next
        return head.value

    def peek(self):
        return self.head.value if self.head else None

    def print(self):
        node = self.head

        while node is not Node:
            print(node.value)
            node = node.next
