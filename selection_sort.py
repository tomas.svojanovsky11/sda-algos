unordered_list = [3, 5, 22, 18, 1, 2]


def selection_sort(unordered_list):
    size = len(unordered_list)

    for i in range(size):  # zelena sipka
        min_index = i

        for j in range(i + 1, size):  # oranzova sipka
            if unordered_list[j] < unordered_list[min_index]:
                min_index = j

        # prohozeni
        temp = unordered_list[i]
        unordered_list[i] = unordered_list[min_index]
        unordered_list[min_index] = temp


selection_sort(unordered_list)

print(unordered_list)
