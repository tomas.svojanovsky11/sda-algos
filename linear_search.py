# pokud nic nenajdu, tak vracim -1
# pokud neco najdu, tak vracim jeho index

# print(len(nums))
# range(0, 9) -> [0, 1, 2, 3, 4, 5, 6, 7, 8]
# range(0, 9) + 1 -> [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

nums = [1, 2, 11, 4, 36, 7, 8, 5, 9]


#       0  1  2   3  4   5  6  7  8

# def linear_search(needle, haystack):  # needle je to co hledam, haystack je list
#     for i in range(len(haystack)):
#         if haystack[i] == needle:
#             return i
#
#     return -1

# def linear_search(needle, haystack):
#     for i, current in enumerate(haystack):
#         if current == needle:
#             return i
#
#     return -1
#
#
# print(linear_search(11, nums))
