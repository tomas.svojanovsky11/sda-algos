grid = [
    [1, 0, 0, 0, 0, 0],
    [1, 0, 0, 1, 1, 0],
    [0, 0, 0, 1, 1, 0],
    [1, 1, 0, 0, 0, 0],
    [1, 0, 0, 0, 1, 1],
    [1, 0, 0, 0, 0, 0],
]


def minimal_island(grid):
    visited = set()

    min_size = float("inf")

    def dfs(grid, row, col, visited):
        is_outside_row = row < 0 or row >= len(grid)
        is_outside_col = col < 0 or col >= len(grid[0])

        # mimo mapu
        if is_outside_row or is_outside_col:
            return 0

        # voda
        if grid[row][col] == 0:
            return 0

        # uz jsem tam byl
        if (row, col) in visited:
            return 0

        visited.add((row, col))

        size = 1

        # grid = [
        #     [1, 0, 0, 0, 0, 0],
        #     [1, 0, 0, 1, 1, 0],
        #     [0, 0, 0, 1, 1, 0],
        #     [1, 1, 0, 0, 0, 0],
        #     [1, 0, 0, 0, 1, 1],
        #     [1, 0, 0, 0, 0, 0],
        # ]

        size += dfs(grid, row - 1, col, visited)
        print(size)
        size += dfs(grid, row + 1, col, visited)
        print(size, "slunicko")
        size += dfs(grid, row, col - 1, visited)
        print(size)
        size += dfs(grid, row, col + 1, visited)
        print(size)

        return size

    for row in range(len(grid)):
        for col in range(len(grid[0])):
            size = dfs(grid, row, col, visited)

            if 0 < size < min_size:
                min_size = size

    return min_size


minimal_island(grid)
