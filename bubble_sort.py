# unordered_list = [3, 5, 22, 18, 1, 2]
unordered_list = [1, 2, 3, 5, 18, 22]


def bubble_sort(unordered_list):
    size = len(unordered_list)
    swapped = False

    for i in range(0, size):
        for j in range(0, size - i - 1):
            if unordered_list[j] > unordered_list[j + 1]:
                temp = unordered_list[j]
                unordered_list[j] = unordered_list[j + 1]
                unordered_list[j + 1] = temp
                swapped = True
        print("test")
                # unordered_list[j], unordered_list[j + 1] = unordered_list[j + 1], unordered_list[j]

        if not swapped:
            break

bubble_sort(unordered_list)
print(unordered_list)
