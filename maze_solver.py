maze = [
    "xxxxxxxxxx x",  # 0
    "x        x x",  # 1
    "x        x x",  # 2
    "x xxxxxxxx x",  # 3
    "x          x",  # 4
    "x xxxxxxxxxx",  # 5
]  # 01234567891011

direction = [
    # x, y
    [-1, 0],  # vlevo
    [1, 0],  # vpravo
    [0, 1],  # nahoru
    [0, -1],  # dolu
]


class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __repr__(self):
        return f"{self.x} {self.y}"


# wall = "#"

def walk(maze, wall, curr, end, seen, path):
    # base case 1 - zed
    if maze[curr.y][curr.x] == wall:
        return False

    # base case 2 - mimo mapu
    if curr.x < 0 or curr.x >= len(maze[0]) or curr.y < 0 or curr.y >= len(maze):
        return False

    # base case 3 - uz jsem tam byl
    if seen[curr.y][curr.x]:
        return False

    # base case 4 - dosel jsem do cile
    if curr.x == end.x and curr.y == end.y:
        path.append(curr)
        return True

    seen[curr.y][curr.x] = True
    path.append(curr)

    for dir in direction:
        x, y = dir

        if walk(maze, wall, Point(curr.x + x, curr.y + y), end, seen, path):
            return True

    path.pop()

    return False


def solve(maze, wall, start, end):
    seen = []
    path = []

    for _ in maze:
        seen.append([False] * len(maze[0]))

    print(seen)

    walk(maze, wall, start, end, seen, path)

    return path


result = solve(maze, "x", Point(10, 0), Point(1, 5))
print(result)


def draw_path(maze, path):
    data = []
    for row in maze:
        data.append(list(row))

    # data = list(map(lambda row: list(row), maze))  # potrebujeme list se znaky misto cely retezec
    # print(data)

    for point in path:  # projdu vysledek a mapuju proti hernimu planku
        data[point.y][point.x] = "O"

    return list(map(lambda row: "".join(row), data))  # prevod znaku do retezce


with_path = draw_path(maze, result)
for row in with_path:
    print(row)
