unordered_list = [3, 5, 22, 18, 1, 2]


def insertion_sort(unordered_list):
    for i in range(1, len(unordered_list)):
        value = unordered_list[i]  # zelena sipka
        j = i - 1  # oranzova sipka

        while j >= 0 and value < unordered_list[j]:
            unordered_list[j + 1] = unordered_list[j]
            j = j - 1

        unordered_list[j + 1] = value


insertion_sort(unordered_list)

print(unordered_list)
